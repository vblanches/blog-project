import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isAuth = false;

  posts = [
    {
      title: "Mon Premier Post1",
      content: "blablablablab",
      loveIts: 0,
      created_at: new Date()
    },

    {
      title: "Mon Deuxième Post",
      content: "blablablablab2",
      loveIts: 0,
      created_at: new Date()
    },

    {
      title: "Mon Troisième Post",
      content: "blablablablab3",
      loveIts: 0,
      created_at: new Date()
    }

    ];

    constructor() {
      setTimeout(
        () => {
          this.isAuth = true;
        }, 2000
      );
    }

}
