import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() postListArrays : any[];

  constructor() { }

  ngOnInit() {
      }

  onLoveIts() {
    // console.log("Love it!");
    this.postListArrays.loveIts++;

  }

  onDontLoveIts() {
    // console.log("Don't love it!");
    this.postListArrays.loveIts--;
  }
  
}
